FROM node:20 as frontend-builder

RUN npm i -g pnpm

WORKDIR /app

COPY package.json .
COPY pnpm-lock.yaml .
RUN pnpm install

COPY . .
RUN pnpm run build

FROM nginx:alpine

COPY --from=frontend-builder /app/build /usr/share/nginx/html
#To copy the right config file which takes into account that the url's don't have .html but the files yes.
COPY ngix.conf /etc/nginx/conf.d/default.conf


EXPOSE 80

